## Postgresql anonymizer CENTOS 7
1) instalar extension usando yum
```
    $ yum install postgresql_anonymizer11
```
2)  Añadir configuracion a postgresql.conf (Reiniciar servicio)
```
shared_preload_libraries = 'anon'
```
3) Tener dos usuarios
* Admin: Este debe tener permisos sobre la base de datos
* limitado: Este es el que vera los datos enmascarados

4) Con el usuario admin activar la maquina de enmascarado
```
CREATE EXTENSION IF NOT EXISTS anon CASCADE;
SELECT anon.start_dynamic_masking();
```
* por defecto start_dynamic_masking se activa en el esquema public, si se desea usar otro se tiene que agregar en los parentesis.  
5) Declarar los usuarios que van a estar enmascarados
```
=# CREATE ROLE limitado LOGIN;
=# SECURITY LABEL FOR anon ON ROLE limitado 
-# IS 'MASKED';
```
6) Declarar las reglas de enmascarado
```
SECURITY LABEL FOR anon ON COLUMN tabla.columna 
IS 'MASKED WITH FUNCTION anon.random_last_name()';
```
* Para mas informacion de las funciones consultar https://postgresql-anonymizer.readthedocs.io/en/latest/masking_functions/

7) Probar los datos enmascarados con los usuarios limitados
```
psql base_de_datos -U limitado -c 'SELECT * FROM tabla;'
```

* Para mas informacion de esta extension consultar https://postgresql-anonymizer.readthedocs.io/en/latest/

8) Usar vistas materializadas
* Las mascaras dinamicas tienen la desventaja de solo funcionar bajo el esquema public
* Se pueden crear vistas materializadas usando las funciones de anon
* https://www.postgresql.org/docs/current/static/sql-creatematerializedview.html
```
CREATE MATERIALIZED VIEW masked_tabla AS
SELECT
    id,
    anon.random_last_name() AS name,
    anon.random_date_between('01/01/1920'::DATE,now()) AS birth,
    fk_last_order,
    store_id
FROM tabla;
```
* Esta opcion si funciona bien con varios esquemas

9) Solucion alterna

* Crear otro esquema y crear vistas de las tablas del esquema de origen

* Darle permisos al usuarios masked de usar ese esquema