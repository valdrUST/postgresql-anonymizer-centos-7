<?php
require_once('connect.php');
class Usuario_total{
    private $db;
    private $schema;
    function __construct($schema){
        $this->db = new Conexion(1);
        $this->schema = $schema;
    }

    function get_usuarios(){
        $res = pg_prepare($this->db->conn, "get_usuarios", "SELECT * FROM ".$this->schema.".usuario");
        $res = pg_fetch_assoc(pg_execute($this->db->conn, "get_usuarios",array()));
        return $res;
    }
}

class Usuario_limit{
    private $db;
    private $schema;
    function __construct($schema){
        $this->db = new Conexion(2);
        $this->schema = $schema;
    }

    function get_usuarios(){
        $res = pg_prepare($this->db->conn, "get_usuarios", "SELECT * FROM usuario");
        $res = pg_fetch_assoc(pg_execute($this->db->conn, "get_usuarios",array()));
        return $res;
    }

    function get_perfil(){
        $res = pg_prepare($this->db->conn, "get_perfil", "SELECT * FROM perfil");
        $res = pg_fetch_assoc(pg_execute($this->db->conn, "get_perfil",array()));
        return $res;
    }
}