<?php
require_once('connect.php');
class dgae_tabla{
    private $db;
    private $schema;
    function __construct($schema){
        $this->db = new Conexion(1);
        $this->schema = $schema;
    }

    function get_tabla(){
        $res = pg_prepare($this->db->conn, "get_tabla", "SELECT * FROM ".$this->schema.".dgae_tabla");
        $res = pg_fetch_assoc(pg_execute($this->db->conn, "get_tabla",array()));
        return $res;
    }
}

class dgae_tabla_anon{
    private $db;
    private $schema;
    function __construct($schema){
        $this->db = new Conexion(2);
        $this->schema = $schema;
    }

    function get_tabla(){
        $res = pg_query($this->db->conn,"SELECT * FROM dgae.dgae_tabla");
        $res = pg_prepare($this->db->conn, "get_tabla", "SELECT * FROM dgae.dgae_tabla");
        $res = pg_fetch_assoc(pg_execute($this->db->conn, "get_tabla",array()));
        return $res;
    }
}